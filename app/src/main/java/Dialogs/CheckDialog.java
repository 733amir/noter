package Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.gitlab.a733amir.noter.noter.R;

import Interfaces.DialogComplete;
import Utilities.Data;

public class CheckDialog extends DialogFragment {

    DialogComplete listener;
    String message;
    Data item;
    String newFolderName;

    Data src;
    String dst;
    boolean copy;

    Data oldFilename, newFilename;

    public CheckDialog setArgs(DialogComplete listener, String message) {
        this.listener = listener;
        this.message = message;
        return this;
    }

    public CheckDialog setItem(Data item) {
        this.item = item;
        return this;
    }

    public CheckDialog setNewFolderName(String name) {
        this.newFolderName = name;
        return this;
    }

    public CheckDialog setSrcDst(Data src, String dst, boolean copy) {
        this.src = src;
        this.dst = dst;
        this.copy = copy;
        return this;
    }

    public CheckDialog setRename(Data src, Data dst) {
        this.oldFilename = src;
        this.newFilename = dst;
        return this;
    }

    public CheckDialog setArgs(DialogComplete listener, int stringId) {
        this.listener = listener;
        this.message = getString(stringId);
        return this;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(message)
                // Add action buttons
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (item != null)
                            listener.dialogDone(item);
                        else if (newFolderName != null)
                            listener.dialogDone(newFolderName);
                        else if (oldFilename != null && newFilename != null)
                            listener.dialogDone(oldFilename, newFilename);
                        else if (src != null && dst != null)
                            listener.dialogDone(src, dst, copy);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        CheckDialog.this.getDialog().cancel();
                        listener.dialogCanceled();
                    }
                });
        return builder.create();
    }
}
