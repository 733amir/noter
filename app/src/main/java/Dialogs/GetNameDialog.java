package Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.gitlab.a733amir.noter.noter.R;

import Interfaces.DialogComplete;

public class GetNameDialog extends DialogFragment {

    DialogComplete listener;
    EditText name_entry;

    public void setArgs(DialogComplete listener) {
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View root = inflater.inflate(R.layout.edittext, null);
        name_entry = root.findViewById(R.id.name_entry);
        name_entry.setSingleLine(true);
        builder.setView(root)
                // Add action buttons
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        listener.dialogDone(name_entry.getText().toString());
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        GetNameDialog.this.getDialog().cancel();
                        listener.dialogCanceled();
                    }
                });
        return builder.create();
    }
}
