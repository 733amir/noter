package Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.gitlab.a733amir.noter.noter.R;

import Interfaces.NotesMenuListener;
import Utilities.Data;
import Utilities.FileManager;

public class RenameDialog extends DialogFragment {

    Data item;
    NotesMenuListener listener;
    FileManager fm;
    EditText name_entry;
    TextInputLayout name_entry_root;

    public void setArgs(NotesMenuListener listener, Data item) {
        this.listener = listener;
        this.item = item;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        fm = FileManager.getInstance(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View root = inflater.inflate(R.layout.edittext, null);

        name_entry = root.findViewById(R.id.name_entry);
        name_entry.setText(item.getName());

        name_entry_root = root.findViewById(R.id.name_entry_root);
        name_entry_root.setHint(getString(R.string.rename));

        builder.setView(root)
                // Add action buttons
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        listener.rename(item, new Data(name_entry.getText().toString(), item.getType()));
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        RenameDialog.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }
}
