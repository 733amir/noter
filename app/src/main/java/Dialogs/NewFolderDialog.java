package Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

import com.gitlab.a733amir.noter.noter.R;

import Interfaces.DialogComplete;
import Interfaces.NotesMenuListener;
import Utilities.FileManager;

public class NewFolderDialog extends DialogFragment {

    NotesMenuListener listener;
    FileManager fm;
    EditText folderName;

    public void setListener(NotesMenuListener listener) {
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        fm = FileManager.getInstance(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View root = inflater.inflate(R.layout.edittext, null);
        folderName = root.findViewById(R.id.name_entry);
        folderName.setSingleLine(true);
        builder.setView(root)
                // Add action buttons
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        if (fm.hasFolder(folderName.getText().toString())) {
                            CheckDialog folderOverwriteApproval = new CheckDialog();
                            folderOverwriteApproval.setArgs((DialogComplete) getActivity(), getString(R.string.folder_overwrite_approval));
                            folderOverwriteApproval.setNewFolderName(folderName.getText().toString());
                            folderOverwriteApproval.show(getActivity().getSupportFragmentManager(), "FolderOverwriteApproval");
                        } else if (!fm.mkdir(folderName.getText().toString(), false))
                            Toast.makeText(getActivity(), getString(R.string.error_new_folder), Toast.LENGTH_SHORT).show();

                        listener.updateList();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        NewFolderDialog.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }
}
