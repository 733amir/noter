package Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.gitlab.a733amir.noter.noter.R;

import java.io.File;

import Interfaces.DialogComplete;
import Interfaces.NotesMenuListener;
import Utilities.Data;
import Utilities.DataType;
import Utilities.FileManager;

public class ItemMenuDialog extends DialogFragment implements DialogInterface.OnClickListener {

    Data item;
    FileManager fm;
    NotesMenuListener listener;
    DialogComplete dialogListener;

    public ItemMenuDialog setItem(Data item) {
        this.item = item;
        return this;
    }

    public ItemMenuDialog setUpdateListener(NotesMenuListener listener) {
        this.listener = listener;
        return this;
    }

    public ItemMenuDialog setDialogListener(DialogComplete listener) {
        this.dialogListener = listener;
        return this;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        fm = FileManager.getInstance(getActivity());

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (item.getType() == DataType.FOLDER)
            builder.setItems(R.array.folder_menu, this);
        else
            builder.setItems(R.array.note_menu, this);

        return builder.create();
    }

    public void onClick(final DialogInterface dialog, int which) {
        DialogProperties properties = new DialogProperties();
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.root = new File(fm.home());
        properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        properties.selection_type = DialogConfigs.DIR_SELECT;
        properties.extensions = null;

        FilePickerDialog folderPickerDialog = new FilePickerDialog(getActivity(), properties);
        folderPickerDialog.setTitle(getString(R.string.select_folder));

        switch (which) {
            case 0: // Delete
                CheckDialog deleteApproval = new CheckDialog();
                deleteApproval.setArgs((DialogComplete) getActivity(), getString(R.string.delete_approval));
                deleteApproval.setItem(item);
                deleteApproval.show(getActivity().getSupportFragmentManager(), "DeleteApprovalDialog");
                break;
            case 1: // Rename
                RenameDialog rename = new RenameDialog();
                rename.setArgs((NotesMenuListener) getActivity(), item);
                rename.show(getActivity().getSupportFragmentManager(), "RenameDialog");
                break;
            case 2: // Move
                folderPickerDialog.setDialogSelectionListener(new DialogSelectionListener() {
                    @Override
                    public void onSelectedFilePaths(String[] files) {
                        listener.move(item, files[0]);
                    }
                });
                folderPickerDialog.show();
                break;
            case 3: // Copy
                folderPickerDialog.setDialogSelectionListener(new DialogSelectionListener() {
                    @Override
                    public void onSelectedFilePaths(String[] files) {
                        listener.copy(item, files[0]);
                    }
                });
                folderPickerDialog.show();
                break;
        }

        listener.updateList();
    }
}
