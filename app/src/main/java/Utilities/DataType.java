package Utilities;

public enum DataType {
    PARENT, FOLDER, FILE, TEXTNOTE, CANVASNOTE
}
