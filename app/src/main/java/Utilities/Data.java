package Utilities;

import java.io.File;

public class Data {
    private String name;
    private DataType type;
    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Data(String name, DataType type) {
        this.name = name;
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(DataType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        if (type == DataType.TEXTNOTE)
            return name + FileManager.md;
        else if (type == DataType.CANVASNOTE)
            return name + FileManager.can;
        return name;
    }

    public DataType getType() {
        return type;
    }

    public String getFormat() {
        if (type == DataType.TEXTNOTE) {
            return FileManager.md;
        } else if (type == DataType.CANVASNOTE) {
            return FileManager.can;
        } else {
            return "";
        }
    }

    public String toString() {
        return getName() + getFormat();
    }
}
