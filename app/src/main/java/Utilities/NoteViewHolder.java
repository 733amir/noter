package Utilities;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gitlab.a733amir.noter.noter.R;

import Interfaces.NotesListClickListener;

public class NoteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
    private NotesListClickListener listener;

    public TextView name;
    public ImageView icon;
    public ImageView option;

    public NoteViewHolder(LinearLayout root, NotesListClickListener listener) {
        super(root);

        this.name = root.findViewById(R.id.note_title);
        this.icon = root.findViewById(R.id.type_icon);
        this.option = root.findViewById(R.id.note_options);
        this.listener = listener;

        root.setOnClickListener(this);
        root.setOnLongClickListener(this);
        option.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.note_options:
                listener.onNoteOptionsClicked(view, getAdapterPosition());
                break;
            default:
                listener.onNoteClicked(view, getAdapterPosition());
        }
    }

    @Override
    public boolean onLongClick(View view) {
        listener.onNoteOptionsClicked(view, getAdapterPosition());
        return true;
    }
}
