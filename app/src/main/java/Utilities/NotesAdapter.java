package Utilities;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.gitlab.a733amir.noter.noter.R;

import Interfaces.NotesListClickListener;

public class NotesAdapter extends RecyclerView.Adapter<NoteViewHolder> {

    private Data[] data;
    private NotesListClickListener listener;

    public NotesAdapter(Data[] dataset, NotesListClickListener listener) {
        setDataset(dataset);
        this.listener = listener;
    }

    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout root = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note, parent, false);

        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        NoteViewHolder vh = new NoteViewHolder(root, listener);
        return vh;
    }

    @Override
    public void onBindViewHolder(NoteViewHolder holder, int position) {
        holder.name.setText(data[position].getName());

        if (data[position].getType() == DataType.PARENT) {
            holder.icon.setImageResource(R.drawable.ic_chevron_left_blue_24dp);
            holder.option.setVisibility(View.GONE);
        } else if (data[position].getType() == DataType.FOLDER) {
            holder.icon.setImageResource(R.drawable.ic_folder_blue_24dp);
            holder.option.setVisibility(View.VISIBLE);
        } else if (data[position].getType() == DataType.TEXTNOTE) {
            holder.icon.setImageResource(R.drawable.ic_text_fields_blue_24dp);
            holder.option.setVisibility(View.VISIBLE);
        } else if (data[position].getType() == DataType.CANVASNOTE) {
            holder.icon.setImageResource(R.drawable.ic_palette_blue_24dp);
            holder.option.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    public void setDataset(Data[] dataset) {
        data = dataset;
        notifyDataSetChanged();
    }

    public Data[] getDataset() {
        return data;
    }
}
