package Utilities;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.snatik.storage.Storage;

import org.zeroturnaround.zip.ZipUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.SortedMap;
import java.util.zip.ZipFile;

/**
 * A singleton class for managing files and folders. This is a stateful file manager inspired by
 * the way bash works.
 */
public class FileManager {

    private static FileManager instance = null;

    private Storage storage;
    private String rootPath;
    private List<String> currentPath;

    public static final String md = ".md", can = ".can";

    private FileManager(Context context) {
        storage = new Storage(context);
        rootPath = storage.getInternalFilesDirectory();
        currentPath = new ArrayList<>(5);
    }

    public static FileManager getInstance(Context context) {
        if (instance == null)
            instance = new FileManager(context);
        return instance;
    }

    public String externalStoragePath() {
        return storage.getExternalStorageDirectory();
    }

    public String home() {
        return storage.getInternalFilesDirectory();
    }

    public String absPath() {
        if (currentPath.isEmpty())
            return rootPath;
        return rootPath + File.separator + TextUtils.join(File.separator, currentPath);
    }

    /**
     * Listing of folders and supported files in current working directory.
     *
     * @return List of files and folders names
     */
    public Data[] ls() {
        List<File> files = storage.getFiles(absPath());
        ArrayList<Data> data_files = new ArrayList<>(0);

        if (!currentPath.isEmpty()) {
            data_files.add(new Data(currentPath.get(currentPath.size() - 1), DataType.PARENT));
        }

        for (int i = 0, j = 0; i < files.size(); ++i, ++j) {
            String filename = files.get(i).getName();
            boolean isFile = files.get(i).isFile();

            if (!isFile)
                data_files.add(new Data(filename, DataType.FOLDER));
            else if (filename.contains(md))
                data_files.add(new Data(filename.substring(0, filename.length() - 3), DataType.TEXTNOTE));
            else if (filename.contains(can))
                data_files.add(new Data(filename.substring(0, filename.length() - 4), DataType.CANVASNOTE));
            else
                --j;
        }
        return data_files.toArray(new Data[]{});
    }

    /**
     * Create an empty file in current working directory.
     *
     * @param filename Name of the file
     * @return true if file created otherwise false
     */
    public boolean touch(String filename) {
        return storage.createFile(absPath() + File.separator + filename, "");
    }

    /**
     * Create a file in current working directory.
     *
     * @param filename Name of the file
     * @param content  Content of the file
     * @param type     Type of file to be created
     * @return true if file created otherwise false
     */
    public boolean touch(String filename, String content, DataType type) {
        String format = (type == DataType.TEXTNOTE ? md : can);
        return storage.createFile(absPath() + File.separator + filename + format, content);
    }

    public boolean touch(String filename, byte[] content, DataType type) {
        String format = (type == DataType.TEXTNOTE ? md : can);
        return storage.createFile(absPath() + File.separator + filename + format, content);
    }

    /**
     * Change current working directory.
     *
     * @param path A relative path from current directory to new directory
     * @return true if current directory changed otherwise false
     */
    public boolean cd(String path) {
        // TODO Add support for absolute paths.
        if (path.equals("..") && currentPath.size() > 0) {
            if (currentPath.size() > 1) {
                currentPath.remove(currentPath.size() - 1);
            } else {
                currentPath.clear();
            }
            return true;
        } else if (storage.isFileExist(absPath() + File.separator + path)) {
            currentPath.add(path);
            return true;
        }
        return false;
    }

    /**
     * Return relative working directory to where internal files directory is.
     *
     * @return path
     */
    public String pwd() {
        if (currentPath.isEmpty())
            return "/";
        else
            return "/" + TextUtils.join(File.separator, currentPath);
    }

    /**
     * Read a text file and return the content.
     *
     * @param path relative path to the file
     * @return content of text file
     */
    public String cat(String path) {
        return storage.readTextFile(absPath() + File.separator + path);
    }

    /**
     * Create a folder with a name in current working directory.
     *
     * @param name Name of the folder
     * @return true if folder is created otherwise false
     */
    public boolean mkdir(String name, boolean overwrite) {
        return storage.createDirectory(absPath() + File.separator + name, overwrite);
    }

    public boolean rm(String path, DataType type) {
        if (type == DataType.FOLDER)
            return storage.deleteDirectory(absPath() + File.separator + path);
        else
            return storage.deleteFile(absPath() + File.separator + path + (type == DataType.TEXTNOTE ? md : can));
    }

    public boolean mv(Data srcItem, String absDstPath) {
        String src = absPath() + File.separator + srcItem;
        String dst = absDstPath + File.separator + srcItem;
        return !src.equals(dst) && storage.move(src, dst);
    }

    public boolean cp(Data srcItem, String absDstPath) {
        String src = absPath() + File.separator + srcItem;
        String dst = absDstPath + File.separator + srcItem;
        return !src.equals(dst) && storage.copy(src, dst);
    }

    public boolean rename(String src, String dst) {
        return storage.rename(absPath() + File.separator + src, absPath() + File.separator + dst);
    }

    public boolean exporting(String path) {
        // TODO this is synchronous operation. Convert it to async and use a loading animation.
        ZipUtil.pack(new File(storage.getInternalFilesDirectory()), new File(path + File.separator + "Noter.zip"));
        return true;
    }

    public boolean importing(String path) {
        ZipUtil.unpack(new File(path), new File(storage.getInternalFilesDirectory()));
        return true;
    }

    public byte[] raw(String path, DataType type) {
        String format = (type == DataType.TEXTNOTE ? md : can);
        return storage.readFile(absPath() + File.separator + path + format);
    }

    public boolean hasFolder(String path) {
        return storage.isDirectoryExists(absPath() + File.separator + path);
    }

    public boolean hasFile(String path) {
        return storage.isFileExist(path);
    }

    public boolean hasDataToExport() {
        return !storage.getFiles(home()).isEmpty();
    }

    public Data[] search(String query) {
        String root = absPath();
        List<File> result = storage.getFiles(root, ".*" + query + ".*(" + md + "|" + can + ")");
        Data[] files = new Data[result.size()];
        for (int i = 0; i < result.size(); i++) {
            File f = result.get(i);
            if (f.getName().contains(md))
                files[i] = new Data(f.getName().substring(0, f.getName().length() - 3), DataType.TEXTNOTE);
            else if (f.getName().contains(can))
                files[i] = new Data(f.getName().substring(0, f.getName().length() - 4), DataType.CANVASNOTE);
        }
        return files;
    }
}
