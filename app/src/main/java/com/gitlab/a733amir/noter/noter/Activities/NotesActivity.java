package com.gitlab.a733amir.noter.noter.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionMenu;
import com.gitlab.a733amir.noter.noter.R;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.io.File;
import java.util.List;

import Dialogs.CheckDialog;
import Dialogs.ItemMenuDialog;
import Dialogs.NewFolderDialog;
import Interfaces.DialogComplete;
import Interfaces.NotesMenuListener;
import Interfaces.NotesListClickListener;
import Utilities.Data;
import Utilities.DataType;
import Utilities.FileManager;
import Utilities.NotesAdapter;

public class NotesActivity extends AppCompatActivity implements NotesListClickListener, NotesMenuListener,
        DialogComplete, MaterialSearchView.OnQueryTextListener, MaterialSearchView.SearchViewListener {

    public static final String filename = "filename";

    private FloatingActionMenu fam;
    private FileManager fm;
    private RecyclerView notesList;
    private NotesAdapter notesAdapter;
    private LinearLayoutManager notesLayoutManager;
    private LinearLayout startupInfo = null;
    private MaterialSearchView searchView = null;
    private Toolbar toolbar = null;
    private TextView noResult = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);

        // Initialization
        fm = FileManager.getInstance(getApplicationContext());
        fam = findViewById(R.id.fam);
        fam.setClosedOnTouchOutside(true);
        notesList = findViewById(R.id.notes_list);
        notesLayoutManager = new LinearLayoutManager(this);
        notesAdapter = new NotesAdapter(fm.ls(), this);
        DividerItemDecoration did = new DividerItemDecoration(notesList.getContext(), notesLayoutManager.getOrientation());
        startupInfo = findViewById(R.id.notes_startup_info);
        searchView = findViewById(R.id.notes_search_view);
        toolbar = findViewById(R.id.notes_toolbar);
        noResult = findViewById(R.id.no_result);

        // Configuration
        setSupportActionBar(toolbar);

        notesList.setHasFixedSize(true);
        notesList.setLayoutManager(notesLayoutManager);
        notesList.setAdapter(notesAdapter);
        notesList.addItemDecoration(did);

        searchView.setOnQueryTextListener(this);
        searchView.setOnSearchViewListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        // Updating notes list because of possible changes.
        updateList();

        // Close floating action menu when user returns to this activity.
        fam.close(false);
    }

    public void newTextNote(View view) {
        // Start a new activity to add a new Text Note.
        startActivity(new Intent(this, TextNoteActivity.class));
    }

    public void newCanvasNote(View view) {
        // Start a new activity to add a new CanvasView Note.
        startActivity(new Intent(this, CanvasNoteActivity.class));
    }

    public void newFolderDialog(View view) {
        NewFolderDialog newFolderDialog = new NewFolderDialog();
        newFolderDialog.setListener(this);
        newFolderDialog.show(getSupportFragmentManager(), "NewFolderDialogFragment");

        fam.close(true);
    }

    public void updateList() {
        // Update list containing notes and folders.
        notesAdapter.setDataset(fm.ls());

        // If there is no data show a dialog about it to user.
        startupInfo.setVisibility(fm.hasDataToExport() ? View.GONE : View.VISIBLE);
    }

    public void updateList(Data[] dataset) {
        // Update list containing notes and folders.
        notesAdapter.setDataset(dataset);

        // If there is no data show a dialog about it to user.
        startupInfo.setVisibility(fm.hasDataToExport() ? View.GONE : View.VISIBLE);
    }

    @Override
    public void onNoteClicked(View view, int position) {
        Data[] files = notesAdapter.getDataset();
        if (files[position].getType() == DataType.PARENT) {
            fm.cd("..");
            updateList();
        } else if (files[position].getType() == DataType.FOLDER) {
            fm.cd(files[position].getName());
            updateList();
        } else if (files[position].getType() == DataType.TEXTNOTE) {
            Intent intent = new Intent(this, TextNoteActivity.class);
            intent.putExtra(filename, files[position].getName());
            startActivity(intent);
        } else if (files[position].getType() == DataType.CANVASNOTE) {
            Intent intent = new Intent(this, CanvasNoteActivity.class);
            intent.putExtra(filename, files[position].getName());
            startActivity(intent);
        }
    }

    @Override
    public void onNoteOptionsClicked(View view, int position) {
        if (fm.ls()[position].getType() != DataType.PARENT) {
            ItemMenuDialog menu = new ItemMenuDialog();
            menu.setItem(fm.ls()[position]).setUpdateListener(this).setDialogListener(this);
            menu.show(getSupportFragmentManager(), "ItemMenuDialogFragment");
        }
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else if (fm.pwd().equals("/"))
            finish();
        else {
            fm.cd("..");
            updateList();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.notes, menu);

        MenuItem item = menu.findItem(R.id.menu_search);
        searchView.setMenuItem(item);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:
                startActivity(new Intent(this, SettingsActivity.class));
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void dialogDone(String data) {
        Log.i("INFO", data);
        if (!fm.rm(data, DataType.FOLDER) || !fm.mkdir(data, false))
            Toast.makeText(this, getString(R.string.error_new_folder), Toast.LENGTH_SHORT).show();
        updateList();
    }

    @Override
    public void dialogDone(Data item) {
        if (!fm.rm(item.getName(), item.getType()))
            Toast.makeText(this, getString(R.string.error), Toast.LENGTH_SHORT).show();
        updateList();
    }

    @Override
    public void dialogCanceled() {
        updateList();
    }

    @Override
    public void dialogDone(Data src, String dst, boolean copy) {
        if (copy) {
            if (!fm.cp(src, dst)) {
                Toast.makeText(this, getString(R.string.error_copy), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.ok_copy), Toast.LENGTH_SHORT).show();
                updateList();
            }
        } else {
            if (!fm.mv(src, dst)) {
                Toast.makeText(this, getString(R.string.error_move), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.ok_move), Toast.LENGTH_SHORT).show();
                updateList();
            }
        }
    }

    @Override
    public void copy(Data src, String dst) {
        if (fm.hasFile(dst + File.separator + src)) {
            CheckDialog replaceApproval = new CheckDialog();
            replaceApproval.setArgs(this, getString(R.string.replaceApproval));
            replaceApproval.setSrcDst(src, dst, true);
            replaceApproval.show(this.getSupportFragmentManager(), "ReplaceApprovalDialog");
        } else
            dialogDone(src, dst, true);
    }

    @Override
    public void move(Data src, String dst) {
        if (fm.hasFile(dst + File.separator + src)) {
            CheckDialog replaceApproval = new CheckDialog();
            replaceApproval.setArgs(this, getString(R.string.replaceApproval));
            replaceApproval.setSrcDst(src, dst, false);
            replaceApproval.show(getSupportFragmentManager(), "ReplaceApprovalDialog");
        } else
            dialogDone(src, dst, false);
    }

    @Override
    public void rename(Data src, Data dst) {
        if ((src.getType() != DataType.FOLDER && fm.hasFile(fm.absPath() + File.separator + dst)) ||
                (src.getType() == DataType.FOLDER && fm.hasFolder(dst.getName()))) {
            CheckDialog replaceApproval = new CheckDialog();
            replaceApproval.setArgs(this, getString(R.string.replaceApproval));
            replaceApproval.setRename(src, dst);
            replaceApproval.show(getSupportFragmentManager(), "ReplaceApprovalDialog");
        } else {
            dialogDone(src, dst);
        }

    }

    @Override
    public void dialogDone(Data src, Data dst) {
        if (src.getType() == DataType.FOLDER && fm.hasFolder(dst.getName())) {
            fm.rm(dst.getFullName(), dst.getType());
            Log.i("INFO", dst.toString());
        }
        fm.rename(src.getFullName(), dst.getFullName());
        updateList();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        // Clear page to view search results
        fam.setVisibility(View.GONE);
        notesAdapter.setDataset(new Data[0]);
        startupInfo.setVisibility(View.GONE);
        searchView.clearFocus();

        // TODO search in files and show the result.
        Data[] res = fm.search(query);
        if (res.length != 0)
            updateList(res);
        else
            noResult.setVisibility(View.VISIBLE);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        noResult.setVisibility(View.GONE);
        return false;
    }

    @Override
    public void onSearchViewShown() {
    }

    @Override
    public void onSearchViewClosed() {
        fam.setVisibility(View.VISIBLE);
        noResult.setVisibility(View.GONE);
        updateList();
    }

    @Override
    public void onStart() {
        super.onStart();
        searchView.closeSearch();
    }
}
