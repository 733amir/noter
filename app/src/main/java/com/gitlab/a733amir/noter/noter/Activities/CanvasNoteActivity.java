package com.gitlab.a733amir.noter.noter.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;
import android.widget.Toast;

import com.android.colorpicker.ColorPickerDialog;
import com.android.colorpicker.ColorPickerSwatch;
import com.android.graphics.CanvasView;
import com.gitlab.a733amir.noter.noter.R;

import java.io.File;

import Dialogs.CheckDialog;
import Dialogs.GetNameDialog;
import Interfaces.DialogComplete;
import Utilities.Data;
import Utilities.DataType;
import Utilities.FileManager;

public class CanvasNoteActivity extends AppCompatActivity implements DialogComplete {

    private CanvasView canvas = null;
    private SeekBar thickness = null;
    private FileManager fm = null;
    private String filename = null;

    public static final int[] colors = new int[]{
            0xff000000, 0xffF6402C, 0xffEB1460, 0xff9C1AB1, 0xff6633B9,
            0xff3D4DB7, 0xff1093F5, 0xff00A6F6, 0xff00BBD5, 0xff009687,
            0xff46AF4A, 0xff88C440, 0xffCCDD1E, 0xffFFEC16, 0xffFFC100,
            0xffFF9800, 0xffFF5505, 0xff7A5547, 0xff9D9D9D, 0xff5E7C8B
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_canvas_note);

        canvas = findViewById(R.id.canvas);
        thickness = findViewById(R.id.thickness);
        fm = FileManager.getInstance(this);

        thickness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int position, boolean fromUser) {
                if (position <= 50) {
                    canvas.setPaintStrokeWidth(3f + 25f * position / 50);
                } else {
                    canvas.setPaintStrokeWidth(27f + 150f * (position - 50) / 50);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // Check for creating new note or opening one for edit.
        filename = getIntent().getStringExtra(NotesActivity.filename);
        if (filename == null) {
            getSupportActionBar().setTitle(getString(R.string.new_canvas));
        } else {
            getSupportActionBar().setTitle(filename);
            canvas.drawBitmap(fm.raw(filename, DataType.CANVASNOTE));
        }
    }

    public void colorPickerClicked(View view) {
        ColorPickerDialog colorPickerDialog = new ColorPickerDialog();
        colorPickerDialog.initialize(R.string.select_color, colors, 1, 4, colors.length);
        colorPickerDialog.show(getSupportFragmentManager(), "ColorPickerDialog");
        colorPickerDialog.setOnColorSelectedListener(new ColorPickerSwatch.OnColorSelectedListener() {
            @Override
            public void onColorSelected(int color) {
                canvas.setPaintStrokeColor(color);
            }
        });
    }

    public void undo(View view) {
        canvas.undo();
    }

    public void redo(View view) {
        canvas.redo();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.canvas_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.canvas_menu_done:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void dialogDone(String data) {
        if (filename == null && fm.hasFile(fm.absPath() + File.separator + data + FileManager.can)) {
            CheckDialog replaceApproval = new CheckDialog();
            replaceApproval.setArgs(this, getString(R.string.replaceApproval));
            replaceApproval.setSrcDst(new Data(data, DataType.CANVASNOTE), fm.absPath(), true);
            replaceApproval.show(getSupportFragmentManager(), "ReplaceApprovalDialog");
        } else {
            if (!fm.touch(data, canvas.getBitmapAsByteArray(), DataType.CANVASNOTE))
                Toast.makeText(this, getString(R.string.error), Toast.LENGTH_SHORT).show();
            super.finish();
        }
    }

    @Override
    public void dialogDone(Data item) {

    }

    @Override
    public void dialogCanceled() {
        super.finish();
    }

    @Override
    public void finish() {
        if (filename == null && !canvas.canUndo()) {
            super.finish();
        } else if (filename == null && canvas.canUndo()) {
            GetNameDialog name = new GetNameDialog();
            name.setArgs(this);
            name.show(getSupportFragmentManager(), "RenameDialog");
        } else {
            dialogDone(filename);
        }
    }

    @Override
    public void dialogDone(Data src, String dst, boolean copy) {
        if (!fm.touch(src.getName(), canvas.getBitmapAsByteArray(), src.getType()))
            Toast.makeText(this, getString(R.string.error), Toast.LENGTH_SHORT).show();
        super.finish();
    }

    @Override
    public void dialogDone(Data src, Data dst) {

    }
}
