package com.gitlab.a733amir.noter.noter.Activities;

import android.Manifest;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.gitlab.a733amir.noter.noter.R;

import java.io.File;
import java.util.List;

import Utilities.FileManager;
import pub.devrel.easypermissions.EasyPermissions;

public class SettingsActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

    private FileManager fm = null;
    private Toolbar toolbar = null;

    private static final int EXTERNAL_STORAGE_REQUEST_EXPORT = 17964, EXTERNAL_STORAGE_REQUEST_IMPORT = 17965;
    private String[] externalStoragePermissions = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // Initializations
        fm = FileManager.getInstance(this);
        toolbar = findViewById(R.id.settings_toolbar);

        // Using Toolbar instead of ActionBar.
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        // Checking for version. Older android version didn't require read permission.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            externalStoragePermissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE};
        else
            externalStoragePermissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
    }

    public void onExportClicked(View view) {
        if (fm.hasDataToExport()) {
            EasyPermissions.requestPermissions(this, getString(R.string.external_storage_access),
                    EXTERNAL_STORAGE_REQUEST_EXPORT, externalStoragePermissions);
        } else {
            Toast.makeText(this, getString(R.string.settings_no_data), Toast.LENGTH_LONG).show();
        }
    }

    public void onImportClicked(View view) {
        EasyPermissions.requestPermissions(this, getString(R.string.external_storage_access),
                EXTERNAL_STORAGE_REQUEST_IMPORT, externalStoragePermissions);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        DialogProperties properties = new DialogProperties();
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.root = new File(fm.externalStoragePath());
        properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = null;

        if (requestCode == EXTERNAL_STORAGE_REQUEST_IMPORT) {
            properties.selection_type = DialogConfigs.FILE_SELECT;

            FilePickerDialog dialog = new FilePickerDialog(this, properties);
            dialog.setTitle(getString(R.string.select_file));
            dialog.setDialogSelectionListener(new DialogSelectionListener() {
                @Override
                public void onSelectedFilePaths(String[] files) {
                    if (!fm.importing(files[0]))
                        Toast.makeText(SettingsActivity.this, getString(R.string.error_import), Toast.LENGTH_SHORT).show();
                    else {
                        Toast.makeText(SettingsActivity.this, getString(R.string.ok_import), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            dialog.show();
        } else if (requestCode == EXTERNAL_STORAGE_REQUEST_EXPORT) {
            properties.selection_type = DialogConfigs.DIR_SELECT;

            FilePickerDialog dialog = new FilePickerDialog(this, properties);
            dialog.setTitle(getString(R.string.select_folder));
            dialog.setDialogSelectionListener(new DialogSelectionListener() {
                @Override
                public void onSelectedFilePaths(String[] files) {
                    if (!fm.exporting(files[0]))
                        Toast.makeText(SettingsActivity.this, getString(R.string.error_export), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(SettingsActivity.this, getString(R.string.ok_export), Toast.LENGTH_SHORT).show();
                }
            });
            dialog.show();

        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        if (requestCode == EXTERNAL_STORAGE_REQUEST_EXPORT || requestCode == EXTERNAL_STORAGE_REQUEST_IMPORT) {
            Toast.makeText(this, getString(R.string.settings_storage_access_denied), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
