package com.gitlab.a733amir.noter.noter.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.Toast;

import com.android.colorpicker.ColorPickerDialog;
import com.android.colorpicker.ColorPickerSwatch;
import com.gitlab.a733amir.noter.noter.R;

import java.io.File;

import Dialogs.CheckDialog;
import Dialogs.GetNameDialog;
import Interfaces.DialogComplete;
import Utilities.Data;
import Utilities.DataType;
import Utilities.FileManager;
import jp.wasabeef.richeditor.RichEditor;

public class TextNoteActivity extends AppCompatActivity implements DialogComplete {

    private FileManager fm;
    private String filename;
    private HorizontalScrollView options;
    private RichEditor editor;

    private final static int scrollX = 10000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_note);

        // Initialization
        fm = FileManager.getInstance(getApplicationContext());

        options = findViewById(R.id.text_customizer_holder);
        options.setSmoothScrollingEnabled(true);
        autoSmoothScroll();

        editor = findViewById(R.id.editor);

        // Check for creating new note or opening one for edit.
        filename = getIntent().getStringExtra(NotesActivity.filename);
        if (filename == null) {
            getSupportActionBar().setTitle(getString(R.string.new_note));
        } else {
            getSupportActionBar().setTitle(filename);
            editor.setHtml(fm.cat(filename + fm.md));
        }

        editor.setPadding(16, 16, 16, 16);
        editor.setPlaceholder(getString(R.string.note));

        findViewById(R.id.action_undo).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.undo();
            }
        });

        findViewById(R.id.action_redo).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.redo();
            }
        });

        findViewById(R.id.action_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.removeFormat();
            }
        });

        findViewById(R.id.action_bold).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.setBold();
            }
        });

        findViewById(R.id.action_italic).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.setItalic();
            }
        });

        findViewById(R.id.action_subscript).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.setSubscript();
            }
        });

        findViewById(R.id.action_superscript).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.setSuperscript();
            }
        });

        findViewById(R.id.action_strikethrough).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.setStrikeThrough();
            }
        });

        findViewById(R.id.action_underline).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.setUnderline();
            }
        });

        findViewById(R.id.action_heading1).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.setHeading(1);
            }
        });

        findViewById(R.id.action_heading2).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.setHeading(2);
            }
        });

        findViewById(R.id.action_heading3).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.setHeading(3);
            }
        });

        findViewById(R.id.action_heading4).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.setHeading(4);
            }
        });

        findViewById(R.id.action_heading5).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.setHeading(5);
            }
        });

        findViewById(R.id.action_heading6).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.setHeading(6);
            }
        });

        findViewById(R.id.action_txt_color).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                ColorPickerDialog colorPickerDialog = new ColorPickerDialog();
                colorPickerDialog.initialize(R.string.select_color, CanvasNoteActivity.colors, 1, 4, CanvasNoteActivity.colors.length);
                colorPickerDialog.show(getSupportFragmentManager(), "ColorPickerDialog");
                colorPickerDialog.setOnColorSelectedListener(new ColorPickerSwatch.OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int color) {
                        TextNoteActivity.this.setTextColor(color);
                    }
                });
            }
        });

        findViewById(R.id.action_bg_color).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                ColorPickerDialog colorPickerDialog = new ColorPickerDialog();
                colorPickerDialog.initialize(R.string.select_color, CanvasNoteActivity.colors, 1, 4, CanvasNoteActivity.colors.length);
                colorPickerDialog.show(getSupportFragmentManager(), "ColorPickerDialog");
                colorPickerDialog.setOnColorSelectedListener(new ColorPickerSwatch.OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int color) {
                        TextNoteActivity.this.setTextBackgroundColor(color);
                    }
                });
            }
        });

        findViewById(R.id.action_indent).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.setIndent();
            }
        });

        findViewById(R.id.action_outdent).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.setOutdent();
            }
        });

        findViewById(R.id.action_align_left).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.setAlignLeft();
            }
        });

        findViewById(R.id.action_align_center).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.setAlignCenter();
            }
        });

        findViewById(R.id.action_align_right).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.setAlignRight();
            }
        });

        findViewById(R.id.action_blockquote).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.setBlockquote();
            }
        });

        findViewById(R.id.action_insert_bullets).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.setBullets();
            }
        });

        findViewById(R.id.action_insert_numbers).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.setNumbers();
            }
        });

        findViewById(R.id.action_insert_image).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
//                editor.insertImage("http://www.1honeywan.com/dachshund/image/7.21/7.21_3_thumb.JPG",
//                        "dachshund");

                // TODO Dialog that gets image url and alt and the insert it.
            }
        });

        findViewById(R.id.action_insert_link).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
//                editor.insertLink("https://github.com/wasabeef", "wasabeef");

                // TODO Dialog that gets link name and url and then insert it.
            }
        });
        findViewById(R.id.action_insert_checkbox).setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                editor.insertTodo();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        options.scrollBy(10000, 0);
    }

    @Override
    public void finish() {
        if (filename == null && (editor.getHtml() == null || editor.getHtml().isEmpty())) {
            super.finish();
        } else if (filename == null && !(editor.getHtml() == null || editor.getHtml().isEmpty())) {
            GetNameDialog name = new GetNameDialog();
            name.setArgs(this);
            name.show(getSupportFragmentManager(), "RenameDialog");
        } else {
            dialogDone(filename);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.text_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.text_menu_done:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void dialogDone(String data) {
        if (filename == null && fm.hasFile(fm.absPath() + File.separator + data + FileManager.md)) {
            CheckDialog replaceApproval = new CheckDialog();
            replaceApproval.setArgs(this, getString(R.string.replaceApproval));
            replaceApproval.setSrcDst(new Data(data, DataType.TEXTNOTE), fm.absPath(), true);
            replaceApproval.show(getSupportFragmentManager(), "ReplaceApprovalDialog");
        } else {
            if (!fm.touch(data, editor.getHtml(), DataType.TEXTNOTE))
                Toast.makeText(this, getString(R.string.error), Toast.LENGTH_SHORT).show();
            super.finish();
        }
    }

    @Override
    public void dialogDone(Data item) {

    }

    @Override
    public void dialogCanceled() {
        super.finish();
    }

    public void setTextColor(int color) {
        editor.setTextColor(color);
    }

    public void setTextBackgroundColor(int color) {
        editor.setTextBackgroundColor(color);
    }

    @Override
    public void dialogDone(Data src, String dst, boolean copy) {
        if (!fm.touch(src.getName(), editor.getHtml(), src.getType()))
            Toast.makeText(this, getString(R.string.error), Toast.LENGTH_SHORT).show();
        super.finish();
    }

    @Override
    public void dialogDone(Data src, Data dst) {

    }

    public void autoSmoothScroll() {
        options.postDelayed(new Runnable() {
            @Override
            public void run() {
                options.scrollBy(10000, 0);
            }
        }, 100);

        options.postDelayed(new Runnable() {
            @Override
            public void run() {
                options.smoothScrollBy(-10000, 0);
            }
        },1000);
    }
}
