package Interfaces;

import Utilities.Data;

public interface DialogComplete {
    public void dialogDone(Data src, String dst, boolean copy);
    public void dialogDone(String data);
    public void dialogDone(Data item);
    public void dialogDone(Data src, Data dst);
    public void dialogCanceled();
}
