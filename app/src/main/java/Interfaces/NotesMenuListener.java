package Interfaces;

import Utilities.Data;

public interface NotesMenuListener {
    public void updateList();
    public void copy(Data src, String dst);
    public void move(Data src, String dst);
    public void rename(Data src, Data dst);
}
