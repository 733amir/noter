package Interfaces;

import android.view.View;

public interface NotesListClickListener {
    void onNoteClicked(View view, int position);
    void onNoteOptionsClicked(View view, int position);
}
