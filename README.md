# Noter

Note taking application that is an assignment for Android Developer position at Cafe Bazaar.

This application support TextNotes base on RichText and Canvas to draw something as note.

## Download

[0.0.5](app/release/Noter-0.0.5.apk)
[0.0.4](app/release/Noter-0.0.4.apk)
[0.0.3](app/release/Noter-0.0.3.apk)
[0.0.2](app/release/Noter-0.0.2.apk)

## TODO

* Add Persian support.
* Add search.
* Add tagging system.
* Solve some compatibility problem because of old WebViewer.

## Changelog

0.0.5:
* Added a search in current directory.
* Added different version for download in README page.
* Added Changelog.